﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsers()
        {
            return Ok(mapper.Map<IEnumerable<UserDTO>>(await userService.GetAll()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetUserById(int id)
        {
            if (id < 1)
                return BadRequest();

            var user = await userService.GetById(id);

            if (user == null)
                return NotFound();
            else
                return Ok(mapper.Map<UserDTO>(user));
        }

        [HttpGet("OrderWithTasks")]
        public async Task<ActionResult<IEnumerable<UserTasksDTO>>> GetOrderedUsersWithOrderedTasks()
        {
            return Ok(mapper.Map<IEnumerable<UserTasksDTO>>(await userService.GetOrderedUsersWithOrderedTasks()));
        }

        [HttpGet("{id}/UserInfo")]
        public async Task<ActionResult<UserInfoDTO>> GetUserInfo(int id)
        {
            if (id < 1)
                return BadRequest();

            var user = await userService.GetById(id);

            if (user == null)
                return NotFound();

            return Ok(mapper.Map<UserInfoDTO>(await userService.GetUserInfo(id)));
        }

        [HttpPost]
        public async Task<IActionResult> AddUsers([FromBody] IEnumerable<ProjectDTO> newUsers)
        {
            if (newUsers.Any(e => !TryValidateModel(e)))
                return BadRequest();

            var users = await userService.GetAll();
            var check = users.Select(e => e.Id)
                .Where(e => newUsers.Select(t => t.Id).Contains(e));

            if (check.Count() > 0)
                return BadRequest("One of the ids exists");

            await userService.Add(mapper.Map<IEnumerable<User>>(newUsers));

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> AddUser([FromBody] ProjectDTO newUser)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (await userService.GetById(newUser.Id) != null)
                return BadRequest("Such id exists");

            await userService.Add(mapper.Map<User>(newUser));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = await userService.GetById(id);

            if (item == null)
                return NotFound();

            try
            {
                await userService.Remove(item);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateUsers([FromBody] IEnumerable<UserDTO> newUsers)
        {
            try
            {
                await userService.Update(mapper.Map<IEnumerable<User>>(newUsers));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateUser([FromBody] UserDTO newUser)
        {
            try
            {
                await userService.Update(mapper.Map<User>(newUser));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}
