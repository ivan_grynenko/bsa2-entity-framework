﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService projectService;
        private readonly IMapper mapper;

        public ProjectsController(IProjectService projectService, IMapper mapper)
        {
            this.projectService = projectService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> GetProjects()
        {
            return Ok(mapper.Map<IEnumerable<ProjectDTO>>(await projectService.GetAll()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetProjectById(int id)
        {
            if (id < 1)
                return BadRequest();

            var project = await projectService.GetById(id);

            if (project == null)
                return NotFound();
            else
                return Ok(mapper.Map<ProjectDTO>(project));
        }

        [HttpGet("withtasks")]
        public async Task<ActionResult<IEnumerable<ProjectInfoDTO>>> GetProjectWithTasks()
        {
            return Ok(mapper.Map<ProjectInfoDTO>(await projectService.GetProjectWithTasks()));
        }

        [HttpPost]
        public async Task<IActionResult> AddProjects([FromBody]IEnumerable<ProjectDTO> newProjects)
        {
            if (newProjects.Any(e => !TryValidateModel(e)))
                return BadRequest();

            var projects = await projectService.GetAll();
            var check = projects.Select(e => e.Id)
                .Where(e => newProjects.Select(t => t.Id).Contains(e));

            if (check.Count() > 0)
                return BadRequest("One of the ids exists");

            await projectService.Add(mapper.Map<IEnumerable<Project>>(newProjects));

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> AddProject([FromBody] ProjectDTO newProject)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (await projectService.GetById(newProject.Id) != null)
                return BadRequest("Such id exists");

            await projectService.Add(mapper.Map<Project>(newProject));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = await projectService.GetById(id);

            if (item == null)
                return NotFound();

            try
            {
                await projectService.Remove(item);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateProjects([FromBody] IEnumerable<ProjectDTO> newProjects)
        {
            try
            {
                await projectService.Update(mapper.Map<IEnumerable<Project>>(newProjects));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateProject([FromBody] ProjectDTO newProject)
        {
            try
            {
                await projectService.Update(mapper.Map<Project>(newProject));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}
