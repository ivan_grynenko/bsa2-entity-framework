﻿using Bogus;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq;

namespace DAL.Configuration
{
    public class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Projects");
            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50);
            builder.Property(e => e.Description)
                .HasMaxLength(200);
            builder.HasOne(e => e.Team)
                .WithMany(e => e.Projects)
                .HasForeignKey(e => e.TeamId)
                .OnDelete(DeleteBehavior.SetNull);
            builder.HasCheckConstraint("CK_Projects_CreatedAt", "[CreatedAt] <= getdate()");
            builder.Property(e => e.Deadline)
                .IsRequired(false);

            var random = new Random();
            var id = 1;
            var fake = new Faker<Project>()
                .RuleFor(p => p.Id, f => id++)
                .RuleFor(p => p.Name, f => f.Random.Word())
                .RuleFor(p => p.TeamId, f => f.Random.Int(1, 5))
                .RuleFor(p => p.AuthorId, f => f.Random.Int(1, 30))
                .RuleFor(p => p.Description, f => f.Lorem.Sentence(random.Next(5, 15)))
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(new DateTime(2017, 1, 1), new DateTime(2020, 1, 1)))
                .RuleFor(p => p.Deadline, f => f.Date.Between(new DateTime(2020, 1, 1), new DateTime(2022, 1, 1)));
            var basicData = Enumerable.Range(1, 10)
                .Select(e => fake.Generate());

            builder.HasData(basicData);
        }
    }
}
