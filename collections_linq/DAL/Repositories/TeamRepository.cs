﻿using DAL.Base;
using DAL.Interfaces;
using DAL.Models;

namespace DAL.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public TeamRepository(ApplicationContext context)
            : base(context)
        { }
    }
}
