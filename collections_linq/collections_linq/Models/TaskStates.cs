﻿using System;

namespace collections_linq.Models
{
    public enum TaskStates
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
