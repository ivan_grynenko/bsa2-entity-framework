﻿using collections_linq.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace collections_linq
{
    class Program
    {
        private static string[] menu =
        {
            "Get a number of tasks of a certain user",
            "Get the list of tasks assigned to a particular user",
            "Get tasks completed in the current year for a particular user",
            "Get the ordered list of teams, which members are 10 years and older",
            "Get the list of users alphabetical order with filtered tasks",
            "Get the User structure",
            "Get the Project structure",
            "Exit"
        };

        static async Task Main(string[] args)
        {
            var worker = new DataWorker();

            while (true)
            {
                Console.Clear();
                int menuPoint = 0;
                ConsoleKeyInfo userInput;

                do
                {
                    DisplayMenu(menuPoint);
                    userInput = Console.ReadKey();

                    if (userInput.Key == ConsoleKey.UpArrow)
                        menuPoint = DisplayMenu(--menuPoint);

                    if (userInput.Key == ConsoleKey.DownArrow)
                        menuPoint = DisplayMenu(++menuPoint);

                } while (userInput.Key != ConsoleKey.Enter);

                if (menu[menuPoint] == "Exit")
                {
                    worker.Dispose();
                    Environment.Exit(0);
                }

                if (menu[menuPoint] == "Get a number of tasks of a certain user")
                {
                    SetInternalDisplay();

                    Console.WriteLine("Enter Id: ");
                    var id = GetNumInput();

                    try
                    {
                        var result = await worker.GetResponse<Dictionary<string, int>>($"api/tasks/{id}/byuser");

                        if (result != null)
                        {
                            foreach (var item in result)
                                Console.WriteLine($"Project: {item.Key}, number: {item.Value}");
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the list of tasks assigned to a particular user")
                {
                    SetInternalDisplay();

                    Console.WriteLine("Enter Id: ");
                    var id = GetNumInput();

                    try
                    {
                        var maxLength = 45;
                        var result = await worker.GetMultipleResponse<Tasks>($"api/tasks/{id}/withmaxlength/{maxLength}");

                        if (result != null)
                        {
                            foreach (var item in result)
                                Console.WriteLine($"Task: {item.Name}");
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get tasks completed in the current year for a particular user")
                {
                    SetInternalDisplay();

                    Console.WriteLine("Enter Id: ");
                    var id = GetNumInput();

                    try
                    {
                        var result = await worker.GetMultipleResponse<Tasks>($"api/tasks/{id}/currentyear");

                        if (result != null)
                        {
                            foreach (var item in result)
                                Console.WriteLine($"Task: {item.Name}");
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the ordered list of teams, which members are 10 years and older")
                {
                    SetInternalDisplay();

                    try
                    {
                        var minAge = 10;
                        var result = await worker.GetMultipleResponse<object>($"api/teams/minage/{minAge}");

                        if (result != null)
                        {
                            foreach (var item in (dynamic)result)
                            {
                                Console.WriteLine($"Id: {item.id}, Team: {item.name}, Memebers:");

                                foreach (var member in item.users)
                                    Console.WriteLine($" - {member.firstName} {member.lastName}");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the list of users alphabetical order with filtered tasks")
                {
                    SetInternalDisplay();

                    try
                    {
                        var result = await worker.GetMultipleResponse<UserTasks>("api/users/orderwithtasks");

                        if (result != null)
                        {
                            foreach (var item in result)
                            {
                                Console.WriteLine($"User: {item.User}, Tasks:");

                                foreach (var task in item.Tasks)
                                    Console.WriteLine($" - Id: {task.Id} {task.Name}");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the User structure")
                {
                    SetInternalDisplay();

                    Console.WriteLine("Enter Id: ");
                    var id = GetNumInput();

                    try
                    {
                        var result = await worker.GetResponse<UserInfo>($"api/users/{id}/userinfo");

                        if (result != null)
                        {
                            Console.WriteLine($"User {result.User?.FirstName} {result.User?.LastName}");
                            Console.WriteLine($"Last project: {result.LastProject?.Name}");
                            Console.WriteLine($"Longest task: {result.LongestTask?.Name}");
                            Console.WriteLine($"Number of tasks: {result.OverallNumOfTasks?.ToString()}");
                            Console.WriteLine($"Incomplete tasks: {result.OverallNumOfCanceledTasks?.ToString()}");
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the Project structure")
                {
                    SetInternalDisplay();

                    try
                    {
                        var result = await worker.GetMultipleResponse<ProjectInfo>("api/projects/withtasks");

                        if (result != null)
                        {
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Project: {item.Project.Id} {item.Project.Name}, " +
                                    $"Longest task: {item.LongestTask.Id} {item.LongestTask.Name}, " +
                                    $"Shortest task: {item.ShortestTask.Id} {item.ShortestTask.Name}");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                Console.WriteLine("\nPress any key ro return");
                Console.ReadKey();
            }
        }

        private static int DisplayMenu(int selectedRow)
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("Use UpArrow and DownArrow keys to navigate. Then press Enter. Select \"Exit\" to close the app." + Environment.NewLine);

            if (selectedRow < 0)
                selectedRow = 0;

            if (selectedRow > menu.Length - 1)
                selectedRow = menu.Length - 1;

            for (int i = 0; i < menu.Length; i++)
            {
                if (i == selectedRow)
                {
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine(menu[i]);
                    Console.ResetColor();
                }
                else
                    Console.WriteLine(menu[i]);
            }

            return selectedRow;
        }

        private static void SetInternalDisplay()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
        }

        private static int GetNumInput()
        {
            var input = Console.ReadLine();
            int result;

            while (!int.TryParse(input, out result))
            {
                Console.WriteLine("It has to be a number. Try again!");
                input = Console.ReadLine();
            }

            return result;
        }

        private static void ErrorHandler(Exception e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(e.Message);
            Console.ResetColor();
        }
    }
}
