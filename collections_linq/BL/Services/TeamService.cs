﻿using BL.Interfaces;
using DAL.Base;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork unitOFWork;

        public TeamService(IUnitOfWork unitOFWork)
        {
            this.unitOFWork = unitOFWork;
        }

        public async Task<IEnumerable<Team>> GetAll()
        {
            return await unitOFWork.TeamRepository.GetAll();
        }

        public async Task<Team> GetById(int id)
        {
            return await unitOFWork.TeamRepository.GetById(id);
        }

        public async Task Add(IEnumerable<Team> entities)
        {
            await unitOFWork.TeamRepository.Add(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Add(Team entity)
        {
            await unitOFWork.TeamRepository.Add(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(IEnumerable<Team> entities)
        {
            unitOFWork.TeamRepository.Remove(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(Team entity)
        {
            unitOFWork.TeamRepository.Remove(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(IEnumerable<Team> entities)
        {
            unitOFWork.TeamRepository.Update(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(Team entity)
        {
            unitOFWork.TeamRepository.Update(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task<IEnumerable<(int? Id, string Name, IEnumerable<User> Users)>> GetTeamsOfCertainAge(int minAge)
        {
            var users = await unitOFWork.UserRepository.GetAll();

            var result = users
                .GroupBy(e => e.TeamId)
                .Where(g => g.All(e => e.Birthday?.AddYears(minAge) <= DateTime.Now) && g.Key != null)
                .Join(await unitOFWork.TeamRepository.GetAll(), u => u.Key, t => t.Id, (u, t) => new { Id = u.Key, t.Name, Users = u.OrderByDescending(e => e.RegisteredAt).AsEnumerable() })
                .Select(e => (e.Id, e.Name, e.Users));

            return result;
        }
    }
}
