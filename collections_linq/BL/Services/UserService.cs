﻿using BL.Interfaces;
using BL.Models;
using DAL.Base;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unitOFWork;

        public UserService(IUnitOfWork unitOFWork)
        {
            this.unitOFWork = unitOFWork;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await unitOFWork.UserRepository.GetAll();
        }

        public async Task<User> GetById(int id)
        {
            return await unitOFWork.UserRepository.GetById(id);
        }

        public async Task Add(IEnumerable<User> entities)
        {
            await unitOFWork.UserRepository.Add(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Add(User entity)
        {
            await unitOFWork.UserRepository.Add(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(IEnumerable<User> entities)
        {
            unitOFWork.UserRepository.Remove(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(User entity)
        {
            unitOFWork.UserRepository.Remove(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(IEnumerable<User> entities)
        {
            unitOFWork.UserRepository.Update(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(User entity)
        {
            unitOFWork.UserRepository.Update(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task<IEnumerable<UserTasks>> GetOrderedUsersWithOrderedTasks()
        {
            var users = await unitOFWork.UserRepository.GetAll();

            var result = users
                .Join(await unitOFWork.TaskRepository.GetAll(), u => u.Id, t => t.PerformerId, (u, t) => new { User = u, Tasks = t })
                .GroupBy(e => e.User)
                .Select(e => new UserTasks { User = e.Key, Tasks = e.Select(t => t.Tasks).OrderByDescending(t => t.Name.Length).AsEnumerable() })
                .OrderBy(e => e.User.FirstName)
                .AsEnumerable();

            return result;
        }

        public async Task<UserInfo> GetUserInfo(int userId)
        {
            var projects = await unitOFWork.ProjectRepository.GetAll();
            var tasks = await unitOFWork.TaskRepository.GetAll();

            var result = projects
                .Where(e => e.AuthorId == userId && e.CreatedAt == projects.Max(e => e.CreatedAt))
                .Take(1)
                .Join(await unitOFWork.TaskRepository.GetAll(), p => p.Id, t => t.ProjectId, (p, t) => new { p, t })
                .GroupBy(e => e.p)
                .Select(e => new
                {
                    LastProject = e.Key,
                    NumOfTasks = e.Count()
                })
                .FirstOrDefault();

            var result2 = tasks
                .Where(e => e.PerformerId == userId)
                .GroupBy(e => e.PerformerId)
                .Select(e => new
                {
                    Incomplete = 1,// e.Where(x => x.State == TaskStates.Canceled || x.State == TaskStates.Started).Count(),
                    Longest = e.Where(x => x.FinishedAt - x.CreatedAt == e.Select(x => x.FinishedAt - x.CreatedAt).Max()).FirstOrDefault()
                })
                .FirstOrDefault();

            return new UserInfo
            {
                User = await unitOFWork.UserRepository.GetById(userId),
                OverallNumOfCanceledTasks = result2?.Incomplete,
                LongestTask = result2?.Longest,
                LastProject = result?.LastProject,
                OverallNumOfTasks = result?.NumOfTasks
            };
        }
    }
}
