﻿using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface ITaskService : IService<Tasks>
    {
        Task<IDictionary<string, int>> GetNumberOfTasksByUser(int userId);
        Task<IEnumerable<Tasks>> GetTasksByUserInCurrentYear(int userId);
        Task<IEnumerable<Tasks>> GetTasksByUserWithMaxNameLength(int userId, int maxLength);
    }
}