﻿using DAL.Models;
using System;
using System.Collections.Generic;

namespace BL.Models
{
    public class UserTasks
    {
        public User User { get; set; }
        public IEnumerable<Tasks> Tasks { get; set; }
    }
}
